import React from "react";
import ReactDOM from "react-dom";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import "./assets/css/index.css";
import "semantic-ui-css/semantic.min.css";
import Home from "./components/Home";
import Menu from "./components/Menu";
import AddQuiz from "./components/AddQuiz";


ReactDOM.render(
  <Router>
    <div>
      <main>
        <Menu />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/AddQuiz" component={AddQuiz} />
        </Switch>
      </main>
    </div>
  </Router>,
  document.getElementById("root")
);
