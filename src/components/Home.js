import React, { PureComponent } from "react";
import { createStore } from "redux";

let store = createStore(counter);

function counter(state = 0, action) {
  switch (action.type) {
    case "INCREMENT":
      return state + 1;
    case "DECREMENT":
      return state - 1;
    default:
      return state;
  }
}

store.subscribe(() => console.log(store.getState()));

class Home extends PureComponent {
  render() {
    return (
      <div>
        <h1>Home</h1>
      </div>
    );
  }
}

export default Home;
