import React, { PureComponent } from "react";
import { Menu } from "semantic-ui-react";
import logo from "./../assets/icons/icon.png";
import { Link } from "react-router-dom";

class MainMenu extends PureComponent {
  render() {
    return (
      <Menu>
        <Menu.Item>
          <img src={logo} alt="WiSo Logo" className="app-icon" />
          WiSo Quiz
        </Menu.Item>
        <Link className="item" to="/" replace >
          home
        </Link>
        <Link className="item" to="/AddQuiz" replace >
          add quiz
        </Link>
      </Menu>
    );
  }
}

export default MainMenu;
