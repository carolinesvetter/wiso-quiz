const { app, BrowserWindow, Menu, shell, ipcMain } = require("electron");
const isDev = require("electron-is-dev");

let mainWindow;

function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    }
  });

  // and load the index.html of the app.
  mainWindow.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "public/index.html")}`
  );

 
  // set up menu template
  const mainMenuTemplate = [
    // Each object is a dropdown
    {
      label: "File",
      submenu: [
        {
          label: "Quit",
          accelerator: process.platform === "darwin" ? "Command+Q" : "Ctrl+Q",
          click() {
            app.quit();
          },
        },
      ],
    },
  ];

  // add dev options
  if (isDev) {
    mainMenuTemplate.push({
      label: "Developer Tools",
      submenu: [
        {
          role: "reload",
          accelerator: "F5",
        },
        {
          label: "Toggle DevTools",
          accelerator: process.platform === "darwin" ? "Command+I" : "F12",
          click(item, focusedWindow) {
            focusedWindow.toggleDevTools();
          },
        },
      ],
    });
  }

  // unshift empty array to avoid empty pixel on mac
  if (process.platform === "darwin") {
    mainMenuTemplate.unshift({});
  }

  // Build menu from template
  const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);

  // Insert menu
  Menu.setApplicationMenu(mainMenu);
}


app.whenReady().then(createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

// On Activate open a new window.
app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
